import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Temps } from '../../api/temps.js';

import './temp.html';
/*
Template.temp.events({
  'click .delete'() {
    Meteor.call('temps.remove', this._id);
  },
});
*/
Template.temp.helpers({
  isOwner() {
    return this.owner === Meteor.userId();
  },
});

Template.temp.events({
  'click .delete'() {
    Meteor.call('temps.remove', this._id);
    },
});

import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Volumes } from '../../api/volumes.js';

import './volume.html';
/*
Template.temp.events({
  'click .delete'() {
    Meteor.call('temps.remove', this._id);
  },
});
*/
Template.volume.helpers({
  isOwner() {
    return this.owner === Meteor.userId();
  },
});


Template.volume.events({
  'click .delete'() {
    Meteor.call('volumes.remove', this._id);
    },
});

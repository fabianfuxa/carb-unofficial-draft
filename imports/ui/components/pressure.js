import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Pressures } from '../../api/pressures.js';

import './pressure.html';
/*
Template.temp.events({
  'click .delete'() {
    Meteor.call('temps.remove', this._id);
  },
});
*/
Template.pressure.helpers({
  isOwner() {
    return this.owner === Meteor.userId();
  },
});


Template.pressure.events({
  'click .delete'() {
    Meteor.call('pressures.remove', this._id);
    },
});

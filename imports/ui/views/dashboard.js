import { Template } from 'meteor/templating';
//import Particle from 'particle-api-js';
import { Temps } from '../../api/temps.js';
import { Volumes } from '../../api/volumes.js';
import { Pressures } from '../../api/pressures.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Meteor } from 'meteor/meteor';
import { AccountsTemplates } from 'meteor/useraccounts:core';

import './dashboard.html';
import '../components/temp.js';
import '../components/volume.js';
import '../components/pressure.js';

/*
Template.dashboard.onCreated(() => {
  Meteor.setInterval(() => {
    //var deviceID = "44001b000d51353532343635";
    //var accessToken = "7fb103ddd15c108784fad5cb9a535c52a511c372";
    //console.log(Meteor.user());
    var deviceID = Meteor.user().photonID;
    var accessToken = Meteor.user().accessToken;
    //console.log(deviceID);
    var varName = "temperature";

    requestURL = "https://api.spark.io/v1/devices/" + deviceID + "/" + varName + "/?access_token=" + accessToken;
    $.getJSON(requestURL, function(json) {
      Meteor.call('temps.insert', json.result.toFixed(2));
    });
  }, 5000);
});
*/

Template.dashboard.helpers({
  /*
  temps() {
    return Temps.find({}, { sort: { createdAt: -1 } });
  },
  volumes() {
    return Volumes.find({}, { sort: { createdAt: -1 } });
  },
  pressures() {
    return Pressures.find({}, { sort: { createdAt: -1 } });
  },
  */
  lastTemp() {
    return Temps.findOne({}, {sort: { createdAt: -1, limit: 1} }).text;
  },
  lastVol() {
    return Volumes.findOne({}, {sort: { createdAt: -1, limit: 1} }).text;
  },
  lastPressure() {
    return Pressures.findOne({}, {sort: { createdAt: -1, limit: 1} }).text;
  },
  username() {
    return Meteor.user().emails[0].address;
  },
  accessToken()  {
    console.log(Meteor.user());
    return Meteor.user().profile.accessToken;
  }
  /*
  pathForHistory() {
    return "google.com";
    //return FlowRouter.go('appBody', {main: 'history'});
  },
  logOut() {
    AccountsTemplates.logOut();
    //Meteor.logout();
    //return FlowRouter.go('appBody', {main: 'login'});
  },
  */
});

Template.dashboard.events({
  'submit .new-temp'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;

    if (text === "" || text === " ") return;

    // Insert a task into the collection
    //Meteor.call('temps.insert', text);
    Meteor.call('temps.insert', text);

    // Clear form
    target.text.value = '';
  },
  'submit .new-volume'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;

    if (text === "" || text === " ") return;

    // Insert a task into the collection
    Meteor.call('volumes.insert', text);

    // Clear form
    target.text.value = '';
  },
  'submit .new-pressure'(event) {
    console.log('ho');
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;

    if (text === "" || text === " ") return;

    // Insert a task into the collection
    //Meteor.call('temps.insert', text);
    Meteor.call('pressures.insert', text);

    // Clear form
    target.text.value = '';
  },
  'click .photon-temp'(event) {
    event.preventDefault();

    console.log("helllo there!");

    //var deviceID = "44001b000d51353532343635";
    //var accessToken = "7fb103ddd15c108784fad5cb9a535c52a511c372";
    var deviceID = Meteor.user().photonID.value;
    var accessToken = Meteor.user().accessToken.value;
    var varName = "temperature";

    requestURL = "https://api.spark.io/v1/devices/" + deviceID + "/" + varName + "/?access_token=" + accessToken;
    $.getJSON(requestURL, function(json) {
      Meteor.call('temps.insert', json.result.toFixed(2));
    });

    //Meteor.call('temps.insert-photon');
  },
  'click .logout-button'(event) {
    event.preventDefault();

    AccountsTemplates.logout();
    //console.log(Meteor.user());

    //FlowRouter.go('login');
    //AccountsTemplates.logout();
  },
  'click .history-button'(event) {
    console.log('ho');
    FlowRouter.go('history');
  },
});

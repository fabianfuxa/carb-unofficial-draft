import { Template } from 'meteor/templating';
//import Particle from 'particle-api-js';
import { Temps } from '../../api/temps.js';
import { Volumes } from '../../api/volumes.js';
import { Pressures } from '../../api/pressures.js';

import { Meteor } from 'meteor/meteor';

import './history.html';
import '../components/temp.js';
import '../components/volume.js';
import '../components/pressure.js';

Template.history.helpers({
  temps() {
    return Temps.find({}, { sort: { createdAt: -1 } });
  },
  volumes() {
    return Volumes.find({}, { sort: { createdAt: -1 } });
  },
  pressures() {
    return Pressures.find({}, { sort: { createdAt: -1 } });
  },
});

import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
//import Particle from 'particle-api-js';
export const Pressures = new Mongo.Collection('pressures');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('pressures', function tasksPublication() {
    return Pressures.find({
      $or: [
        { owner: this.userId },
      ],
    });
  });
}

Meteor.methods({
  'pressures.insert'(text) {
    check(text, String);

    Pressures.insert({
      text,
      createdAt: new Date(),
      email: Meteor.user().emails[0].address,
      owner: Meteor.userId(),
    });
  },
  'pressures.remove'(taskId) {
    check(taskId, String);

    Pressures.remove(taskId);
  },
  'pressures.insert-photon'() {
    console.log("this is where I want the thing to be");

    //var particle = new Particle();
    //particle.callFunction({ deviceId: '44001b000d51353532343635', name: 'led', argument: 'on', auth: '7fb103ddd15c108784fad5cb9a535c52a511c372' });
  }
});

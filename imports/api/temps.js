import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
//import Particle from 'particle-api-js';
export const Temps = new Mongo.Collection('temps');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('temps', function tasksPublication() {
    return Temps.find({
      $or: [
        { owner: this.userId },
      ],
    });
  });
}

Meteor.methods({
  'temps.insert'(text) {
    check(text, String);

    Temps.insert({
      text,
      createdAt: new Date(),
      email: Meteor.user().emails[0].address,
      owner: Meteor.userId(),
    });
  },
  'temps.remove'(taskId) {
    check(taskId, String);

    Temps.remove(taskId);
  },
  'temps.insert-photon'() {
    console.log("this is where I want the thing to be");

    //var particle = new Particle();
    //particle.callFunction({ deviceId: '44001b000d51353532343635', name: 'led', argument: 'on', auth: '7fb103ddd15c108784fad5cb9a535c52a511c372' });
  }
});

import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
//import Particle from 'particle-api-js';
export const Volumes = new Mongo.Collection('volumes');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('volumes', function tasksPublication() {
    return Volumes.find({
      $or: [
        { owner: this.userId },
      ],
    });
  });
}

Meteor.methods({
  'volumes.insert'(text) {
    check(text, String);
    console.log(text);

    Volumes.insert({
      text,
      createdAt: new Date(),
      email: Meteor.user().emails[0].address,
      owner: Meteor.userId(),
    });
  },
  'volumes.remove'(taskId) {
    check(taskId, String);

    Volumes.remove(taskId);
  },
  'volumes.insert-photon'() {
    console.log("this is where I want the thing to be");

    //var particle = new Particle();
    //particle.callFunction({ deviceId: '44001b000d51353532343635', name: 'led', argument: 'on', auth: '7fb103ddd15c108784fad5cb9a535c52a511c372' });
  }
});

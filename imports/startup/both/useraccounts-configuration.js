import { AccountsTemplates } from 'meteor/useraccounts:core';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Meteor } from 'meteor/meteor';
//import { Particle } from 'particle-api-js';
// var Particle = require('particle-api-js');
//
// var particle = new Particle();
// var access_token = "";

function loginFunc() {
  FlowRouter.go('dashboard');
}

function logoutFunc() {
  FlowRouter.go('/');
}

function subscribeToPhoton(userId, info) {
  /*
  console.log(info);
  var deviceID = info.photonID;
  var accessToken = info.accessToken;
  var varName = "temperature";

  requestURL = "https://api.spark.io/v1/devices/" + deviceID + "/" + varName + "/?access_token=" + accessToken;
  $.getJSON(requestURL, function(json) {
    Meteor.call('temps.insert', json.result.toFixed(2));
  });
  */
}
/**
 * The useraccounts package must be configured for both client and server to work properly.
 * See the Guide for reference (https://github.com/meteor-useraccounts/core/blob/master/Guide.md)
 */

function getNewAccessToken() {
  return 123;
}

Accounts.onLogin((user) => {
  //var particle = new Particle();
  //particle.login
  var newAccessToken = getNewAccessToken();

  Meteor.users.update(user.user._id, {$set: {"profile.accessToken": newAccessToken}});
});

Accounts.onCreateUser(function (options, user) {
  //var particle = new Particle();

  // We still want the default hook's 'profile' behavior.
  if (options.profile)
    user.profile = options.profile;

  return user;
});

AccountsTemplates.configure({
  showForgotPasswordLink: true,
  defaultTemplate: 'Auth_page',
  defaultLayout: 'appBody',
  defaultContentRegion: 'main',
  defaultLayoutRegions: {},

  homeRoutePath: '/dashboard',
  onLogoutHook: logoutFunc,
  postSignUpHook: subscribeToPhoton,
});

AccountsTemplates.configureRoute('signIn', {
  name: 'signin',
  path: '/signin',
});

AccountsTemplates.configureRoute('signUp', {
  name: 'join',
  path: '/join',
});

AccountsTemplates.configureRoute('forgotPwd');

AccountsTemplates.configureRoute('resetPwd', {
  name: 'resetPwd',
  path: '/reset-password',
});

if (Meteor.isServer){
    Meteor.methods({
        "userExists": function(username){
            return !!Meteor.users.findOne({username: username});
        },
        "photonExists": function(photonID){
            return !!Meteor.users.findOne({photonID: photonID});
        },
        "accessTokenExists": function(accessToken){
            return !!Meteor.users.findOne({accessToken: accessToken});
        },
    });
}

AccountsTemplates.addFields([
  {
    _id: 'username',
    type: 'text',
    required: true,
    func: function(value){
        if (Meteor.isClient) {
            console.log("Validating username...");
            var self = this;
            Meteor.call("userExists", value, function(err, userExists){
                if (!userExists)
                    self.setSuccess();
                else
                    self.setError(userExists);
                self.setValidating(false);
            });
            return;
        }
        // Server
        return Meteor.call("userExists", value);
    },
  },
  {
    _id: 'photonID',
    type: 'text',
    required: true,
    func: function(value) {
        if (Meteor.isClient) {
            console.log("Validating photonID...");
            var self = this;
            Meteor.call("photonExists", value, function(err, photonExists) {
                if (!photonExists)
                    self.setSuccess();
                else
                    self.setError(userExists);
                self.setValidating(false);
            });
            return;
        }
        // Server
        return Meteor.call("photonExists", value);
    },
  },
  {
    _id: 'accessToken',
    type: 'text',
    required: true,
    func: function(value){
        if (Meteor.isClient) {
            console.log("Validating username...");
            var self = this;
            Meteor.call("accessTokenExists", value, function(err, accessTokenExists){
                if (!accessTokenExists)
                    self.setSuccess();
                else
                    self.setError(accessTokenExists);
                self.setValidating(false);
            });
            return;
        }
        // Server
        return Meteor.call("accessTokenExists", value);
    },
  }
]);

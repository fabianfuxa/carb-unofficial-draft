/*
import { Router } from 'meteor/iron:router';
import '../../ui/views/register.js';
import '../../ui/views/login.js';
import '../../ui/views/dashboard.js';
import '../../ui/views/history.js';

Router.route('/', function () {
  this.render('register');
});

Router.route('/register');

Router.route('/login', function () {
  this.render('login');
});

Router.route('/dashboard');
Router.route('/history');
*/
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
//import { Meteor } from 'meteor/meteor';
//import { Accounts } from 'useraccounts';
import '../../ui/layouts/appBody.js';
import '../../ui/views/login.js';
import '../../ui/views/dashboard.js';
import '../../ui/views/history.js';

//import '../../ui/views/register.js';
//import '../../ui/views/login.js';
//import '../../ui/views/dashboard.js';
//import '../../ui/views/history.js';

//import '../../ui/accounts/accounts-templates.js';

/*
FlowRouter.route('/', {
  action() {
    BlazeLayout.render('register');
  }
});

FlowRouter.route('/login', {
  name: 'login',
  action() {
    BlazeLayout.render('login');
  }
});

FlowRouter.route('/register', {
  name: 'register',
  action() {
    BlazeLayout.render('register');
  }
});

FlowRouter.route('/dashboard', {
  name: 'dashboard',
  action() {
    BlazeLayout.render('dashboard');
  }
});

FlowRouter.route('/history', {
  name: 'history',
  action() {
    BlazeLayout.render('history');
  }
});
*/

FlowRouter.route('/', {
  name: 'appBody',
  action() {
    BlazeLayout.render('appBody', {main: 'login'});
  },
});

FlowRouter.route('/history', {
  name: 'history',
  action() {
    BlazeLayout.render('appBody', {main: 'history'});
  },
});

FlowRouter.route('/login', {
  name: 'login',
  action() {
    BlazeLayout.render('appBody', {main: 'login'});
  }
});

FlowRouter.route('/dashboard', {
  name: 'dashboard',
  action() {
    BlazeLayout.render('appBody', {main: 'dashboard'});
  }
});
/*
FlowRouter.route('/history', {
  name: 'history',
  action() {
    BlazeLayout.render('appBody', {main: 'history'});
  },
});
*/
